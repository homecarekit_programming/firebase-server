package com.homecarekit.snoring;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.homecarekit.snoring.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by user on 2017-11-13.
 */

public class ServerHelper {

    Map<String, Object> map_docData = null;
    ArrayList<String> list_ppg = null;
    ArrayList<String> list_breath = null;
    ArrayList<String> list_acc_x = null;
    ArrayList<String> list_acc_y = null;
    ArrayList<String> list_acc_z = null;
    public class UserProfile{
        String id;
        String pw;
        public UserProfile()
        {

        }
        public UserProfile(String _id, String _pw)
        {
            id     = _id;
            pw = _pw;
        }
        public String getId()
        {
            return id;
        }
        public String getPw(){
            return pw;
        }
    }
    private static final int RESOLVE_CONNECTION_REQUEST_CODE = 1;
    AppCompatActivity _appActivity;
    FirebaseFirestore mDB ;
    UserProfile _profile;
    private static ServerHelper instance = new ServerHelper();

    public static ServerHelper getInstance() {
        return instance;
    }

    void initalize(AppCompatActivity _appActivity) {
        this._appActivity = _appActivity;
        mDB = FirebaseFirestore.getInstance();
        FirebaseFirestore.setLoggingEnabled(true);

        map_docData = new HashMap<>();
        list_ppg = new ArrayList<>();
        list_breath = new ArrayList<>();
        list_acc_x = new ArrayList<>();
        list_acc_y = new ArrayList<>();
        list_acc_z = new ArrayList<>();
    }
    void initInfo(String _name, String _age, String _height, String _weight)
    {

    }

    void login(final String _id, final String _pw) {
        //id duple check
        mDB.collection("user-profile")
                .whereEqualTo("id", _id)
                .get()

                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult().size() == 0)
                            {
                                loginFail("ID를 확인해 주세요");
                            }
                            else
                            {
                                for (DocumentSnapshot document : task.getResult()) {
                                    //loginSuccess();
                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                    if(document.getData().get("password").equals(md5(_pw))) {
                                        _profile = new UserProfile(_id, _pw);
                                        loginSuccess();
                                        break;
                                    }

                                }
                            }

                            loginFail("비밀번호를 확인해 주세요");

                        } else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());

                        }
                    }
                });
    }
    void loginSuccess()
    {
        Toast.makeText(_appActivity.getApplicationContext(), "로그인에 성공하였습니다.", Toast.LENGTH_SHORT).show();

        //startActivity;

    }
    void loginFail(String _msg){
        Toast.makeText(_appActivity.getApplicationContext(), "로그인에 실패하였습니다." + _msg, Toast.LENGTH_SHORT).show();
    }

    void signup(final String _id, final String _pw)
    {
        mDB.collection("user-profile")
                .whereEqualTo("id", _id)
                .get()

                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            if(task.getResult().size() == 0)
                            {
                                Map<String, Object> data = new HashMap<>();
                                data.put("id", _id);
                                data.put("password", md5(_pw));

                                mDB.collection("user-profile")
                                        .add(data)
                                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                            @Override
                                            public void onSuccess(DocumentReference documentReference) {
                                                Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                                                signupSuccess();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.w(TAG, "Error adding document", e);
                                                signupFail("");
                                            }
                                        });
                            }
                            else
                            {
                                for (DocumentSnapshot document : task.getResult()) {
                                    signupFail("존재하는 ID 입니다.");
                                    break;
                                }
                            }


                        } else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });


//        Map<String, Object> data = new HashMap<>();
//        data.put("id", _id);
//        data.put("password", md5(_pw));
//
//        mDB.collection("user-profile")
//                .add(data)
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.w(TAG, "Error adding document", e);
//                    }
//                });

    }
    void signupFail(String _msg)
    {
        Toast.makeText(_appActivity.getApplicationContext(), "회원 가입에 실패하였습니다." + _msg, Toast.LENGTH_SHORT).show();

    }
    void signupSuccess()
    {
        Toast.makeText(_appActivity.getApplicationContext(), "회원 가입에 성공하였습니다.", Toast.LENGTH_SHORT).show();
    }

    void readySend()
    {
            map_docData.clear();
            list_ppg.clear();
            list_breath.clear();
            list_acc_x.clear();
            list_acc_y.clear();
            list_acc_z.clear();
    }
    /*
    * 10ms * 4
    * */
    void pushDataPPG(String _ppg_1, String _ppg_2, String _ppg_3, String _ppg_4)
    {
        list_ppg.add(_ppg_1);
        list_ppg.add(_ppg_2);
        list_ppg.add(_ppg_3);
        list_ppg.add(_ppg_4);
    }
    /*
    * 20ms * 2
    * */
    void pushDataBreath(String _breath_1, String _breath_2)
    {
        list_breath.add(_breath_1);
        list_breath.add(_breath_2);
    }
    /*
    * 20ms * 2
    * */
    void pushDataAcc(String _acc_x_1, String _acc_x_2, String _acc_y_1, String _acc_y_2, String _acc_z_1, String _acc_z_2)
    {
        list_acc_x.add(_acc_x_1);
        list_acc_x.add(_acc_x_2);

        list_acc_y.add(_acc_y_1);
        list_acc_y.add(_acc_y_2);

        list_acc_z.add(_acc_z_1);
        list_acc_z.add(_acc_z_2);
    }

    void sendDataToServer()
    {
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String timestamp = simpleDateFormat.format(now);
        map_docData.put("id" , "ehdrua0305");//_profile.getId());
        map_docData.put("date", timestamp);
        map_docData.put("arr_ppg", list_ppg);
        map_docData.put("arr_breath", list_breath);
        map_docData.put("arr_acc_x", list_acc_x);
        map_docData.put("arr_acc_y", list_acc_y);
        map_docData.put("arr_acc_z", list_acc_z);

        mDB.collection("data").add(map_docData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                        Toast.makeText(_appActivity.getApplicationContext(), "데이터를 성공적으로 전송하였습니다." , Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });


    }

    void getData()
    {
        mDB.collection("data")
                .whereEqualTo("id", "ehdrua0305")//_profile.getId())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                                for (DocumentSnapshot document : task.getResult()) {
                                    //loginSuccess();
                                    Log.d("getData Test", document.getId() + " => " + document.getData());

                                }

                        } else {
                            Log.d("getData Test", "Error getting documents: ", task.getException());

                        }
                    }
                });
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
